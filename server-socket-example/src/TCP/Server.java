package TCP;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private ServerSocket serverSocket;
    private List<ObjectInputStream> input;
    private List<ObjectOutputStream> output;
    private List<Socket> clients;
    boolean isWaiting = false;

    public Server() {
        try {
            serverSocket = new ServerSocket(11111);
        } catch (IOException e) {
            System.err.println("Critical error : server creation.");
            e.printStackTrace();
            System.exit(1);
        }

        input = new ArrayList<>();
        output = new ArrayList<>();
        clients = new ArrayList<>();
    }

    public void acceptConnection() {
        try {
            Socket newS = serverSocket.accept();
            clients.add(newS);
            output.add(new ObjectOutputStream(newS.getOutputStream()));
            input.add(new ObjectInputStream(newS.getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendToI(Object o, int i) {
        try {
            output.get(i).reset();
            output.get(i).writeObject(o);
            output.get(i).flush();
        } catch (Exception e) {
            System.err.println("No connection : impossible to send.");
            e.printStackTrace();
        }
    }

    public void sendToAll(Object o) {
        for (ObjectOutputStream out : output) {
            try {
                out.reset();
                out.writeObject(o);
                out.flush();
            } catch (Exception e) {
                System.err.println("No connection : impossible to send.");
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public String getFromI(int i) {
        try {
            Object o = input.get(i).readObject();
            if ( o instanceof String)
            	return (String) o;
        } catch (Exception e) {
            System.err.println("No Connection : can't receive from server.");
            e.printStackTrace();
        }
        return null;
    }

}
