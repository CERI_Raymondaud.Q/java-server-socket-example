package TCP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
    private Socket socket;
    private ObjectInputStream input;
    private ObjectOutputStream output;

    public Client(String ip) {
        try {
            socket = new Socket(ip, 11111);
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            System.err.println("Critical error : client creation.");
            e.printStackTrace();
        }
    }

    public void send(String msg) {
        try {
            output.reset();
            output.writeObject(msg);
            output.flush();
        } catch (Exception e) {
            System.err.println("No Connection : can't send to server.");
            e.printStackTrace();
        }
    }

    public String getMsg() {
        try {
            Object o = input.readObject();
            if ( o instanceof String)
            	return (String) o;
        } catch (Exception e) {
            System.err.println("No Connection : can't receive from server.");
            e.printStackTrace();
        }
        return null;
    }
}
