package TCP;
import java.util.Scanner;

public class Main {
    static int nbClient = 0;
	
    public static void main(String[] args) {
        System.out.println("TCP MESSENGER TEST");
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Server or Cli [S/C] :?");
        String choice = myObj.nextLine();  // Read user input
        myObj.close();
        if ( choice.equals("S") || choice.equals("s") || choice.equals("") ){
	    	
            Server serv = new Server();
            Thread accepter = new Thread() {
                public void run() {
                    while (true){
                        serv.acceptConnection();
                        serv.sendToI("Welcome in da hood ", nbClient);
                        serv.sendToAll("New Client");
                        System.out.println(serv.getFromI(nbClient));
                        nbClient += 1;
                    }
                }
            };
            accepter.run();  	
        }
	    
        else {
            Client cli = new Client("localhost");
            cli.send("Hello imma cli ! ");
            while(true)
                System.out.println(cli.getMsg());
        }
    }
}